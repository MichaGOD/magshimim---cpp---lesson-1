//linkedArray.cpp

#include <iostream>
#include "stack.h"
#include "linkedArray.h"

// will initiate the stack.
void initStack(stack* s)
{
	s->head = new linkedArray;
	s->head->next = NULL;
}

// will push a variable to the stack (at the start);
void push(stack* s, unsigned int element)
{
	linkedArray* arrNode = s->head;
	while (arrNode->next != NULL)
	{
		arrNode = arrNode->next;
	}
	arrNode->next = new linkedArray;
	arrNode->next->_value = element;
	arrNode->next->next = NULL;
}

// will pop the last variable that the stack recieved in a push.
int pop(stack* s)
{
	linkedArray* arrNode = s->head;
	if (arrNode == NULL)
	{
		return -1;
	}
	while (arrNode->next != NULL && arrNode->next->next != NULL)
	{
		arrNode = arrNode->next;
	}
	int returnedValue = arrNode->next->_value;
	delete arrNode->next;
	arrNode->next = NULL;
	return returnedValue;
}

// will clear the stack.
void cleanStack(stack* s)
{
	linkedArray* temp = s->head;
	while (temp->next != NULL)
	{
		s->head = s->head->next;
		delete temp;
		temp = s->head;
	}
	delete temp;
	s->head = NULL;
}