//queue.cpp

#include <iostream>
#include "queue.h"

/*
Function that will initiate the queue.
*/
void initQueue(queue* q, unsigned int size)
{
	q->_maxSize = size;
	q->_count = 0;
	q->_elements = new unsigned int[size];
}

/*
Function that clears the queue.
*/
void cleanQueue(queue* q)
{
	delete[] q->_elements;
}

/*
Function that inserts a value to our queue.
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->_count < q->_maxSize)
	{
		q->_elements[q->_count] = newValue;
		q->_count++;
	}
}

/*
Function that returns the Top element in the queue, or -1 if queue is empty.
*/
int dequeue(queue* q)
{
	int topQueue = 0;
	if (q->_count == 0)
	{
		return -1;
	}
	topQueue = q->_elements[0];
	for (int i = 1; i < q->_count; i++)
	{
		q->_elements[i - 1] = q->_elements[i];
	}
	q->_count--;
	return topQueue;
}

/*
Function that checks if the queue is full, if so returns true, else - returns false.
*/
bool isFull(queue* q)
{
	if (q->_count == q->_maxSize)
	{
		return true;
	}
	return false;
}

/*
Function that checks if the queue is empty, if so returns true, else - returns false.
*/
bool isEmpty(queue* q)
{
	if (q->_count == 0)
	{
		return true;
	}
	return false;
}