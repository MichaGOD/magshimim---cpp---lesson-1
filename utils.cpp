//utils.cpp

#include <iostream>
#include "utils.h"
#include "stack.h"

//this function will reverse the array.
void reverse(int* nums, unsigned int size)
{
	stack* s = new stack;
	initStack(s);
	for (int i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}
	for (int i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}
}

//this function will reverse an array of 10 numbers that we get from input.
int* reverse10()
{
	int* arr = new int[ARR_MAX];
	for (int i = 0; i < ARR_MAX; i++)
	{
		std::cout << "Enter Element " << i + 1 << ": ";
		std::cin >> arr[i];
	}
	reverse(arr, ARR_MAX);
	return arr;
}