//linkedArray.cpp

#include <iostream>
#include "linkedArray.h"


// will add a certain value to the linked array (to the first pos).
void addToLinkedArr(linkedArray** arr, unsigned int value)
{
	linkedArray* temp = new linkedArray;
	temp->next = *arr;
	temp->_value = value;
	(*arr) = temp;
}

// will remove the last link form the linked array.
void removeFromLinkedArr(linkedArray** arr)
{
	(*arr) = (*arr)->next;
}