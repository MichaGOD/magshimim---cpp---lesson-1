#include <iostream>

#ifndef LINKEDARRAY_H
#define LINKEDARRAY_H


/* a linked array that contains positive integer values. */

typedef struct linkedArray
{
	unsigned int _value;
	linkedArray* next;

} linkedArray;

/* 
Function that adds to the linked array something (in the beginning).
*/
void addToLinkedArr(linkedArray** arr, unsigned int value);

/*
Function that deletes the last thing in the linked array.
*/
void removeFromLinkedArr(linkedArray** arr);


#endif /* LINKEDARRAY_H */
